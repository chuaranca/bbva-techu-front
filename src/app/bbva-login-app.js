import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

import '@polymer/iron-form/iron-form';
import '@vaadin/vaadin-button/vaadin-button';
import '@vaadin/vaadin-ordered-layout/vaadin-vertical-layout';
import '@vaadin/vaadin-form-layout/vaadin-form-layout';
import '@vaadin/vaadin-text-field/vaadin-text-field';
import '@vaadin/vaadin-text-field/vaadin-number-field';
import '@vaadin/vaadin-text-field/vaadin-password-field';
import '@vaadin/vaadin-notification/vaadin-notification';

import '../styles/shared-styles';

import * as axios from 'axios';
import { URL_LOGIN } from '../utils/constants';
import { sessionRegisterJwt } from '../utils/utils';
class BbvaLoginApp extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
        }

        .body__container {
          height: 100vh;
          width: 100vw;
          display: flex;
          justify-content: center;
          align-items: center;
          flex-direction: column;
        }

        .login__card {
          padding: var(--lumo-space-m);
          border: 1px solid var(--lumo-contrast-10pct);
          border-radius: var(--lumo-border-radius);
          background: var(--lumo-base-color);
          width: 20em;
          height: 35em;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        .login__avatar {
          width: 10em;
        }

        vaadin-button {
          margin-top: var(--lumo-space-m);
        }
      </style>

      <div class="body__container">
        <div class="login__card">
          <img class="login__avatar" src="../assets/images/login-avatar.png" />

          <h2>Bbva - Log in</h2>

          <iron-form id="form">
            <form>
              <vaadin-vertical-layout>
                <vaadin-form-layout>
                  <vaadin-number-field
                    label="Código de Empresa"
                    required
                    value="{{codigoEmpresa}}"
                    error-message="Favor de ingresar el Código de Empresa."
                  ></vaadin-number-field>

                  <vaadin-text-field
                    label="Código de Usuario"
                    required
                    value="{{codigoUsuario}}"
                    error-message="Favor de ingresar el Código de Usuario."
                  ></vaadin-text-field>

                  <vaadin-password-field
                    label="Password"
                    required
                    value="{{password}}"
                    error-message="Favor de ingresar el Password."
                  ></vaadin-password-field>

                  <vaadin-button theme="primary" on-click="_login"
                    >Acceder
                  </vaadin-button>
                </vaadin-form-layout>
              </vaadin-vertical-layout>
            </form>
          </iron-form>
        </div>
      </div>

      <vaadin-notification 
        opened="{{loadingStatus}}"
        duration="2000"
        position="top-center">
        <template>
          <div>
            <p><b>Log in</b></p>
            <p>Usuario autenticado correctamente...</p>
          </div>
        </template>
      </vaadin-notification>

      <vaadin-notification opened="{{formInvalidOpen}}">
        <template>
          <div>
            <p><b>Campos Inválidos</b></p>
            <p>
              Por favor, completar todos los campos requeridos e intentar
              nuevamente.
            </p>
          </div>
        </template>
      </vaadin-notification>
    `;
  }

  static get is() {
    return 'bbva-login-app';
  }

  static get properties() {
    return {
      codigoEmpresa: { type: String, value: '00000001' },
      codigoUsuario: { type: String, value: 'ADMIN001' },
      password: { type: String, value: '123456' },
      loadingStatus: { type: Boolean, value: false },
      formInvalidOpen: { type: Boolean, value: false }
    };
  }

  _login() {
    if (this.$.form.validate()) {
      var codigos = `${this.codigoEmpresa}|${this.codigoUsuario}`;

      axios
        .post(URL_LOGIN, {
          codigo: codigos,
          password: this.password
        })
        .then(res => {
          sessionRegisterJwt(res.data.token);
          this.dispatchEvent(new CustomEvent('_loginOk', {
            bubbles: true,
            composed: true
          }));
          this.loadingStatus = true;
        })
        .catch(err => console.info(err));
    } else {
      this.formInvalidOpen = true;
    }
  }
}

customElements.define(BbvaLoginApp.is, BbvaLoginApp);
