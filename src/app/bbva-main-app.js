import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';
import './bbva-starter-app';
import './bbva-login-app';

class BbvaMainApp extends PolymerElement {
  static get template() {
    return html`
      <template is="dom-if" if="[[!authenticated]]">
        <bbva-login-app></bbva-login-app>
      </template>
      <template is="dom-if" if="[[authenticated]]">
        <bbva-starter-app unresolved user-session="[[userSession]]">
          Bbva Techu App
        </bbva-starter-app>
      </template>
    `;
  }

  static get is() {
    return 'bbva-main-app';
  }

  static get properties() {
    return {
      authenticated: { type: Boolean, value: false },
      userSession: { type: Object }
    };
  }

  ready() {
    super.ready();
    this.validateSession();
    this.addEventListener('_loginOk', this._loginOk);
  }

  validateSession() {
    const userSession = sessionStorage.getItem('userData');
    if (userSession) {
      this.authenticated = true;
      this.userSession = JSON.parse(userSession);
    }
  }

  _loginOk() {
    this.authenticated = true;
  }
}

customElements.define(BbvaMainApp.is, BbvaMainApp);
