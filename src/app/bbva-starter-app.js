import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';
import { setPassiveTouchGestures } from '@polymer/polymer/lib/utils/settings.js';
import '@vaadin/vaadin-lumo-styles/color.js';
import '@vaadin/vaadin-lumo-styles/spacing.js';
import '@vaadin/vaadin-lumo-styles/typography.js';
import '@vaadin/vaadin-app-layout/vaadin-app-layout.js';
import '@vaadin/vaadin-app-layout/vaadin-drawer-toggle.js';
import '@vaadin/vaadin-tabs/vaadin-tabs.js';
import '@vaadin/vaadin-tabs/vaadin-tab.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '../styles/layout-styles.js';
import {
  HOME,
  USER_MGMT,
  GLOBAL_POSITION,
  NEW_ACCOUNT,
  TRANSFERS,
  T_CHANGE
} from '../routes/urls';
import { onLocationChanged } from '../routes/utils';
import { TOKEN } from '../utils/constants.js';

/**
 * Starter application shell.
 *
 * @class BbvaStarterApp
 * @extends {PolymerElement}
 */
class BbvaStarterApp extends PolymerElement {
  static get template() {
    return html`
      <style include="lumo-typography">
        :host {
          display: block;
        }

        [main-title] {
          padding: var(--lumo-space-m) 0;
          font-size: var(--lumo-font-size-xl);
          line-height: var(--lumo-line-height-m);
          font-weight: 400;
        }

        section {
          display: flex;
          flex-direction: column;
          height: 100%;
        }

        .dark-mode {
          margin: auto var(--lumo-space-m) var(--lumo-space-m);
        }
      </style>

      <vaadin-app-layout>
        <!-- Navbar content -->
        <vaadin-drawer-toggle slot="navbar"></vaadin-drawer-toggle>
        <div main-title slot="navbar">
          <slot></slot>
        </div>

        <!-- Drawer content -->
        <section slot="drawer">
          <vaadin-tabs
            selected="{{selected}}"
            orientation="vertical"
            aria-controls="mainContent"
          >
          <vaadin-tab>
              <a href="/home">Portada</a>
            </vaadin-tab>
            <vaadin-tab>
              <a href="/global-position">Posición Global</a>
            </vaadin-tab>
            <vaadin-tab>
              <a href="/transfers">Transferencias</a>
            </vaadin-tab>
            <vaadin-tab>
              <a href="/t-change">T-Cambio</a>
            </vaadin-tab>
            <vaadin-tab>
              <a href="/new-account">Apertura de Cuentas</a>
            </vaadin-tab>
            <hr></hr>
            <vaadin-tab>
              <a href="/user-mgmt">Gestión de Usuarios</a>
            </vaadin-tab>
          </vaadin-tabs>
          <vaadin-checkbox checked="{{dark}}" class="dark-mode">
            Usar modo oscuro
          </vaadin-checkbox>
          <vaadin-button
            theme="error"
            on-click="_logout">
            Cerrar Sesión
          </vaadin-button>
        </section>

        <!-- Main content -->
        <main aria-live="polite" id="mainContent">
          <!-- view content -->
        </main>
      </vaadin-app-layout>
    `;
  }

  static get is() {
    return 'bbva-starter-app';
  }

  static get properties() {
    return {
      selected: Number,
      dark: {
        type: Boolean,
        observer: '_darkChanged'
      }
    };
  }

  constructor() {
    super();
    setPassiveTouchGestures(true);
  }

  ready() {
    super.ready();
    this.removeAttribute('unresolved');
    onLocationChanged(this.__onRouteChanged.bind(this));
    import(/* webpackChunkName: "router" */ '../routes/router.js').then(
      router => {
        router.init(this.shadowRoot.querySelector('main'));
      }
    );
  }

  _darkChanged(dark, oldDark) {
    if (dark) {
      document.documentElement.setAttribute('theme', 'dark');
    } else {
      document.documentElement.removeAttribute('theme');
    }
  }

  __onRouteChanged(e) {
    switch (e.detail.location.pathname) {
      case HOME:
        this.selected = 0;
        break;
      case GLOBAL_POSITION:
        this.selected = 1;
        break;
      case TRANSFERS:
        this.selected = 2;
        break;
      case T_CHANGE:
        this.selected = 3;
        break;
      case NEW_ACCOUNT:
        this.selected = 4;
        break;
      case USER_MGMT:
        this.selected = 5;
        break;
      default:
        this.selected = null;
    }
  }

  _logout() {
    console.info('_logout');
    sessionStorage.removeItem('userData');
    sessionStorage.removeItem(TOKEN);
    location.reload();
  }
}

customElements.define(BbvaStarterApp.is, BbvaStarterApp);
