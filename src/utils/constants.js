const URL_BASE_BACK = 'http://127.0.0.1:3333/api/v1';

/** API CONSTANST */
export const URL_LOGIN = `${URL_BASE_BACK}/empresas/login`;
export const URL_USERS = `${URL_BASE_BACK}/usuarios`;
export const URL_PRODUCTS = `${URL_BASE_BACK}/productos`;
export const URL_GET_MOVEMENTS = `${URL_BASE_BACK}/operacion`;
export const URL_SIM_TRANSFERS = `${URL_BASE_BACK}/operacion/simulartranspropia`;
export const URL_MAKE_TRANSFERS = `${URL_BASE_BACK}/operacion/realizartranspropia`;
export const URL_SIM_TCHANGE = `${URL_BASE_BACK}/operacion/simulartcambio`;
export const URL_MAKE_TCHANGE = `${URL_BASE_BACK}/operacion/realizartCambio`;

/** RESPONSE STATUS CODE */
export const STATUS_SUCCESS = 'SUCCESS';
export const STATUS_WARNING = 'WARNING';
export const STATUS_ERROR = 'ERROR';
export const MSG_ERROR = 'Servicio Temporalmente no disponible.';

/** GLOBAL CONSTANTS */
export const TOKEN = 'JWT';
export const TOKEN_KEY = 'Ch4y4nn3012';
