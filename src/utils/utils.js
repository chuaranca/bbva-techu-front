import * as jwt from 'jsonwebtoken';
import { TOKEN, TOKEN_KEY } from './constants';

/**
 * Función encargada de decodificar el JWT y registrarlo en el session storage
 * @param jwtCode - Código JWT.
 */
export function sessionRegisterJwt(token) {
  const decoded = jwt.verify(token, TOKEN_KEY);
  sessionStorage.setItem('userData', JSON.stringify(decoded.user));
  sessionStorage.setItem(TOKEN, token);
}

export function verifyToken() {
  let returnValue = true;
  const token = sessionStorage.getItem(TOKEN);
  if (token) {
    jwt.verify(token, TOKEN_KEY, (err, decode) => {
      if (err) {
        sessionStorage.removeItem(TOKEN);
        sessionStorage.removeItem('userData');
        returnValue = false;
      }
    });
  } else {
    returnValue = false;
  }
  return returnValue;
}
