import { Router } from '@vaadin/router';
import {
  HOME,
  USER_MGMT,
  GLOBAL_POSITION,
  NEW_ACCOUNT,
  TRANSFERS,
  T_CHANGE
} from './urls';
import { verifyToken } from '../utils/utils';

export function init(outlet) {
  const router = new Router(outlet);
  router.setRoutes([
    {
      path: '/',
      redirect: HOME
    },
    {
      path: HOME,
      component: 'bbva-home-view',
      action: () => {
        if (verifyToken()) {
          import('../views/bbva-home-view');
        } else {
          location.reload();
        }
      }
    },
    {
      path: GLOBAL_POSITION,
      component: 'bbva-gp-view',
      action: () => {
        if (verifyToken()) {
          import('../views/bbva-gp-view');
        } else {
          location.reload();
        }
      }
    }, 
    {
      path: TRANSFERS,
      component: 'bbva-transfers-view',
      action: () => {
        if (verifyToken()) {
          import('../views/bbva-transfers-view');
        } else {
          location.reload();
        }
      }
    },
    {
      path: T_CHANGE,
      component: 'bbva-tchange-view',
      action: () => {
        if (verifyToken()) {
          import('../views/bbva-tchange-view');
        } else {
          location.reload();
        }
      }
    },
    {
      path: NEW_ACCOUNT,
      component: 'bbva-newaccount-view',
      action: () => {
        if (verifyToken()) {
          import('../views/bbva-newaccount-view');
        } else {
          location.reload();
        }
      }
    },
    {
      path: USER_MGMT,
      component: 'bbva-usermgmt-view',
      action: () => {
        if (verifyToken()) {
          import('../views/bbva-usermgmt-view');
        } else {
          location.reload();
        }
      }
    },
    {
      path: '(.*)+',
      component: 'app-404',
      action: () => {
        import(/* webpackChunkName: "404" */ '../views/404');
      }
    }
  ]);
}
