export const EMPLOYEE_LIST = '/employee-list';
export const NEW_EMPLOYEE = '/employee-new';

export const HOME = '/home';
export const GLOBAL_POSITION = '/global-position';
export const TRANSFERS = '/transfers';
export const T_CHANGE = '/t-change';
export const NEW_ACCOUNT = '/new-account';
export const USER_MGMT = '/user-mgmt';