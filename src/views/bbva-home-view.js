import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

import '../styles/shared-styles.js';

class BbvaHomeView extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
        }

        .text__align {
          text-align: center;
        }

        .img__home {
          width: 85em;
        }
      </style>

      <div class="card">
        <h2>BBVA - TechU</h2>
        <hr></hr>

        <h3 class="text__align">Bienvenido: {{username}}</h3>

        <img class="img__home" src="../assets/images/bbva-home.jpeg" />
      </div>
    `;
  }

  static get is() {
    return 'bbva-home-view';
  }

  static get properties() {
    return {
      username: String
    }
  }

  ready() {
    super.ready();
    const userData = JSON.parse(sessionStorage.getItem('userData'));
    this.username = `${userData.codigoUsuario} - ${userData.nombres}`;
    console.info(this.username);
  }
}

customElements.define(BbvaHomeView.is, BbvaHomeView);
