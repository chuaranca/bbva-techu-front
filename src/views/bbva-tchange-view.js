import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

import '@polymer/iron-form/iron-form';
import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-ordered-layout/vaadin-vertical-layout';
import '@vaadin/vaadin-text-field/vaadin-number-field';
import '@vaadin/vaadin-select/vaadin-select';
import '@vaadin/vaadin-button/vaadin-button';
import '@vaadin/vaadin-combo-box/vaadin-combo-box';
import '@vaadin/vaadin-notification/vaadin-notification';

import '../styles/shared-styles';
import * as axios from 'axios';
import {
  URL_SIM_TCHANGE,
  TOKEN,
  MSG_ERROR,
  URL_PRODUCTS,
  URL_MAKE_TCHANGE,
  STATUS_SUCCESS
} from '../utils/constants';
class BbvaTChangeView extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
        }

        .amount__title {
          color: var(--lumo-primary-color);
        }

        .font-size-xl {
          font-size: var(--lumo-font-size-xl);
        }
      </style>
      
      <div class="card">
        <template is="dom-if" if="[[!isFormSimulationTChange]]">
          <h2>T-Cambio</h2>
          <hr></hr>
          <h4 style="text-align: center">Necesito...</h4>
          <iron-form id="form">
            <form>
              <vaadin-form-layout>
                <vaadin-number-field
                  label="Importe"
                  has-controls
                  placeholder="1"
                  required
                  min="1"
                  value="{{amount}}"
                  error-message="Favor de Ingresar un importe válido"
                ></vaadin-number-field>

                <vaadin-select
                  label="Moneda" 
                  required
                  error-message="Favor de seleccionar el tipo de moneda."
                  value="{{currency}}"
                  on-change="_changeCurrency">
                  <template>
                    <vaadin-list-box>
                      <vaadin-item value="PEN">Soles</vaadin-item>
                      <vaadin-item value="USD">Dólares</vaadin-item>
                    </vaadin-list-box>
                  </template>
                </vaadin-select>

                <vaadin-button
                  colspan="2"
                  on-click="_simulationTChange"
                  >Convertir de {{currencies.from}} a {{currencies.to}}
                </vaadin-button>
              </vaadin-form-layout>
            </form>
          </iron-form>
        </template>

        <template is="dom-if" if="[[isFormSimulationTChange]]">
          <h2>T-Cambio - Simulación</h2>
          <hr></hr>
          <iron-form id="form">
            <form>
              <vaadin-form-layout>
                <vaadin-text-field
                  label="Importe"
                  readonly
                  value="{{tchange.quantity}}"
                >
                  <div slot="prefix">{{tchange.source}}</div>
                </vaadin-text-field>

                <vaadin-text-field
                  label="Tipo de Cambio"
                  readonly
                  value="{{tchange.value}}"
                >
                </vaadin-text-field>

                <vaadin-text-field
                  label="Total"
                  colspan="2"
                  readonly
                  value="{{tchange.amount}} {{tchange.target}}"
                >
                </vaadin-text-field>

                <vaadin-combo-box
                  label="Cuenta Origen"
                  required
                  items="[[accountsCo]]"
                  value="{{accountCo}}"
                  error-message="Favor de seleccionar la cuenta de origen"
                ></vaadin-combo-box>
                <vaadin-combo-box
                  label="Cuenta Destino"
                  required
                  items="[[accountsCd]]"
                  value="{{accountCd}}"
                  error-message="Favor de seleccionar la cuenta de destino"
                ></vaadin-combo-box>

                <vaadin-button
                  colspan="2"
                  on-click="_confirmTchange"
                  >Aceptar
                </vaadin-button>
                <vaadin-button
                  colspan="2"
                  theme="error"
                  on-click="_cancelTchange"
                  >Cancelar
                </vaadin-button>
              </vaadin-form-layout>
            </form>
          </iron-form>
        </template>
      </div>

      <vaadin-notification
        opened="{{notificationOpen}}"
        duration="2000"
        position="top-center"
      >
        <template>
          <div>
            <p><b>T - Cambio</b></p>
            <p>{{message}}</p>
          </div>
        </template>
      </vaadin-notification>
    `;
  }

  static get is() {
    return 'bbva-tchange-view';
  }

  static get properties() {
    return {
      currency: { type: String, value: 'USD' },
      amount: Number,
      currencies: {
        type: Object,
        value: {
          from: 'SOLES',
          from_g: 'PEN',
          to: 'DÓLARES',
          to_g: 'USD'
        }
      },
      isFormSimulationTChange: { type: Boolean, value: false },
      tchange: {
        type: Object,
        value: {
          source: '',
          target: '',
          value: 0,
          quantity: 0,
          amount: 0
        }
      },
      notificationOpen: Boolean,
      message: String,
      accountsCo: { type: Array, value: [] },
      accountsCd: { type: Array, value: [] }
    };
  }

  _changeCurrency() {
    if (this.currency === 'PEN') {
      this.currencies = {
        from: 'DÓLARES',
        from_g: 'USD',
        to: 'SOLES',
        to_g: 'PEN'
      };
    } else {
      this.currencies = {
        from: 'SOLES',
        from_g: 'PEN',
        to: 'DÓLARES',
        to_g: 'USD'
      };
    }
  }

  _loadAccounts() {
    console.info('_loadAccounts()');
    const token = sessionStorage.getItem(TOKEN);
    axios
      .get(URL_PRODUCTS, {
        headers: {
          'access-token': token
        }
      })
      .then(res => {
        const listco = [];
        const listcd = [];
        if (res.status === 200) {
          const target = this.tchange.target;
          res.data.forEach(product => {
            if (product.divisa === target) {
              listco.push(
                `${product.numeroCuenta} - ${product.tipo} - ${product.importeDisponible} - ${product.divisa}`
              );
            } else {
              listcd.push(
                `${product.numeroCuenta} - ${product.tipo} - ${product.importeDisponible} - ${product.divisa}`
              );
            }
          });
          this.accountsCo = listco;
          this.accountsCd = listcd;
        }
      })
      .catch(err => console.info(err));
  }

  _simulationTChange() {
    this.isFormSimulationTChange = true;
    const token = sessionStorage.getItem(TOKEN);
    axios
      .post(
        URL_SIM_TCHANGE,
        {
          divisaOrigen: this.currencies.from_g,
          divisaDestino: this.currencies.to_g,
          importe: this.amount
        },
        {
          headers: {
            'access-token': token
          }
        }
      )
      .then(res => {
        if (res.status === 200) {
          this.tchange = res.data;
          this._loadAccounts();
        }
      })
      .catch(err => {
        this.message = MSG_ERROR;
        this.notificationOpen = true;
      });
  }

  _confirmTchange() {
    console.info('_confirmTchange');
    const token = sessionStorage.getItem(TOKEN);
    axios
      .post(
        URL_MAKE_TCHANGE,
        {
          cuentaOrigen: this.accountCo.split('-')[0].trim(),
          cuentaDestino: this.accountCd.split('-')[0].trim(),
          importeOperacion: this.tchange.quantity,
          importeTCambio: this.tchange.amount,
          tipoDeCambio: this.tchange.value,
          divisaDestino: this.tchange.source
        },
        {
          headers: {
            'access-token': token
          }
        }
      )
      .then(res => {
        if (res.status === 200) {
          this.message = res.data.message;
          this.notificationOpen = true;
          if (res.data.status === STATUS_SUCCESS) this._cancelTchange();
        }
      })
      .catch(err => {
        this.message = MSG_ERROR;
        this.notificationOpen = true;
      });
  }

  _cancelTchange() {
    this.amount = '';
    this.currency = 'USD';
    this.isFormSimulationTChange = false;
  }
}

customElements.define(BbvaTChangeView.is, BbvaTChangeView);
