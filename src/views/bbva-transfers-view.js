import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

import '@vaadin/vaadin-form-layout/vaadin-form-layout';
import '@vaadin/vaadin-ordered-layout/vaadin-vertical-layout';
import '@vaadin/vaadin-date-picker/vaadin-date-picker';
import '@vaadin/vaadin-combo-box/vaadin-combo-box';
import '@vaadin/vaadin-text-field/vaadin-number-field';
import '@vaadin/vaadin-button/vaadin-button';
import '@vaadin/vaadin-dialog/vaadin-dialog';
import '@vaadin/vaadin-lumo-styles/icons';
import '@polymer/iron-icon/iron-icon';
import '@vaadin/vaadin-icons/vaadin-icons';

import '../styles/shared-styles';
import * as axios from 'axios';
import {
  TOKEN,
  URL_PRODUCTS,
  URL_SIM_TRANSFERS,
  MSG_ERROR,
  STATUS_WARNING,
  URL_MAKE_TRANSFERS
} from '../utils/constants';

class BbvaTransfersView extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
        }

        #btn-acept {
          margin-top: 1em;
        }

        .amount__title {
          color: var(--lumo-primary-color);
        }

        .font-size-xl {
          font-size: var(--lumo-font-size-xl);
        }

        .font-size-l {
          font-size: var(--lumo-font-size-l);
        }
      </style>

      <div class="card">
        <template is="dom-if" if="[[!transferSimulation]]">
          <h2>Transferencias Propias</h2>
          <hr></hr>
          <iron-form id="form">
            <form>
              <vaadin-form-layout>
                <vaadin-combo-box
                  label="Cuenta de Cargo"
                  required
                  items="[[accounts]]"
                  value="{{accountCc}}"
                  error-message="Favor de seleccionar la cuenta de cargo"
                ></vaadin-combo-box>
                <vaadin-combo-box
                  label="Cuenta de Abono"
                  required
                  items="[[accounts]]"
                  value="{{accountCa}}"
                  error-message="Favor de seleccionar la cuenta de abono"
                ></vaadin-combo-box>

                <vaadin-number-field
                  label="Importe" 
                  required
                  value="{{amount}}">
                </vaadin-number-field>

                <vaadin-date-picker
                  readonly
                  label="Fecha de Operación"
                  value="[[operationDate]]"
                ></vaadin-date-picker>

                <vaadin-button id="btn-acept" colspan="2" on-click="_submitForm"
                  >Aceptar
                </vaadin-button>
                <vaadin-button theme="error" colspan="2" on-click="_cleanForm"
                  >Cancelar
                </vaadin-button>
              </vaadin-form-layout>
            </form>
          </iron-form>
        </template>

        <template is="dom-if" if="[[transferSimulation]]">
          <h2>Transferencias Propias - Simulación</h2>
          <hr></hr>
          <div class="card">
            <vaadin-form-layout>
              <label class="amount__title font-size-xl">Importe a Transferir</label>
              <label class="font-size-xl">
                {{dataSimulation.importeOperacion}} {{dataSimulation.divisa}}
              </label>

              <label>Número de Cuenta de Cargo: <b class="font-size-l">{{dataSimulation.cuentaOrigen}}</b></label>
              <label>Número de Cuenta de Abono: <b class="font-size-l">{{dataSimulation.cuentaDestino}}</b></label>
              
              <vaadin-button on-click="_confirmTransfer">
                Confirmar
              </vaadin-button>
              <vaadin-button theme="error" on-click="_cancelTransfer">
                Cancelar
              </vaadin-button>
            </vaadin-form-layout>
          </div>
          
        </template>
      </div>

      <vaadin-notification
        opened="{{notificationOpen}}"
        duration="2000"
        position="top-center"
      >
        <template>
          <div>
            <p><b>Transferencias Propias</b></p>
            <p>{{message}}</p>
          </div>
        </template>
      </vaadin-notification>
    `;
  }

  static get is() {
    return 'bbva-transfers-view';
  }

  static get properties() {
    return {
      accounts: {
        type: Array,
        value: () => []
      },
      accountCc: { type: String },
      accountCa: { type: String },
      amount: { type: Number },
      operationDate: {
        type: String,
        value: () => {
          const date = new Date();
          return `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
        }
      },
      notificationOpen: { type: Boolean },
      message: { type: String },
      transferSimulation: { type: Boolean, value: false },
      dataSimulation: { type: Object }
    };
  }

  ready() {
    super.ready();
    this.addEventListener('onload', this._loadAccounts(event));
  }

  _loadAccounts() {
    console.info('_loadAccounts()');
    const jwt = sessionStorage.getItem(TOKEN);
    const accountsAux = [];
    axios
      .get(URL_PRODUCTS, {
        headers: {
          'access-token': jwt
        }
      })
      .then(res => {
        if (res.status === 200) {
          for (let i = 0; i < res.data.length; i++) {
            const product = res.data[i];
            accountsAux.push(
              `${product.numeroCuenta} - ${product.tipo} - ${product.importeDisponible} - ${product.divisa}`
            );
          }
          this.accounts = accountsAux;
        }
      })
      .catch(err => console.info(err));
  }

  _submitForm() {
    console.info('_submitForm()');
    const token = sessionStorage.getItem(TOKEN);

    axios
      .post(
        URL_SIM_TRANSFERS,
        {
          cuentaOrigen: this.accountCc.split('-')[0].trim(),
          cuentaDestino: this.accountCa.split('-')[0].trim(),
          importeOperacion: parseInt(this.amount),
          divisa: this.accountCa.split('-')[3].trim()
        },
        {
          headers: {
            'access-token': token
          }
        }
      )
      .then(res => {
        if (res.status === 200) {
          if (res.data.status === STATUS_WARNING) {
            this.message = res.data.message;
            this.notificationOpen = true;
          } else {
            this.dataSimulation = res.data;
            this.transferSimulation = true;
          }
        }
      })
      .catch(err => {
        this.message = MSG_ERROR;
        this.notificationOpen = true;
      });
  }

  _confirmTransfer() {
    console.info('_confirmTransfer()');
    const token = sessionStorage.getItem(TOKEN);
    console.info(this.dataSimulation);
    axios
      .post(URL_MAKE_TRANSFERS, this.dataSimulation, {
        headers: {
          'access-token': token
        }
      })
      .then(res => {
        if (res.status === 200) {
          this.message = res.data.message;
          this.notificationOpen = true;
          this.transferSimulation = false;
          this._cleanForm();
        }
      })
      .catch(err => {
        this.message = MSG_ERROR;
        this.notificationOpen = true;
      });
  }

  _cleanForm() {
    this.accountCc = '';
    this.accountCa = '';
    this.amount = '';
  }

  _cancelTransfer() {
    this.transferSimulation = false;
    this._cleanForm();
  }
}

customElements.define(BbvaTransfersView.is, BbvaTransfersView);
