import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

import '@polymer/iron-form/iron-form';
import '@vaadin/vaadin-radio-button/vaadin-radio-group';
import '@vaadin/vaadin-radio-button/vaadin-radio-button';
import '@vaadin/vaadin-accordion/vaadin-accordion';
import '@vaadin/vaadin-ordered-layout/vaadin-vertical-layout';
import '@vaadin/vaadin-details/vaadin-details';
import '@vaadin/vaadin-button/vaadin-button';
import '@vaadin/vaadin-select/vaadin-select';
import '@vaadin/vaadin-dialog/vaadin-dialog.js';

import '../styles/shared-styles.js';
import * as axios from 'axios';
import { URL_PRODUCTS, TOKEN, MSG_ERROR } from '../utils/constants.js';

class BbvaNewAccountView extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
        }

        vaadin-button {
          margin-top: 1em;
          width: 100%;
        }

        vaadin-select { 
          width: 50%;
        }

        .justify__content {
          text-align: justify;
        }
      </style>
      <div class="card">
        <iron-form id="form">
          <form>
            <h2>Apertura de Cuentas 100% Online</h2>
            <hr></hr>
          
            <vaadin-accordion>  
              <vaadin-accordion-panel theme="reverse">
                <div slot="summary">1. Elección de una cuenta</div>  
                <vaadin-radio-group id="type"
                theme="vertical"
                required
                error-message="Favor de elegir un tipo de cuenta."
                value="{{type}}">
                  <vaadin-radio-button value="CF" checked>
                    <vaadin-details>
                      <div slot="summary">Cuenta Fácil</div>
                      <ul class="justify__content">
                          <li><b>Mundo Independencia: </b>Todas las cuentas con saldo podrán acceder a los descuentos en restaurantes y otros establecimientos.</li>
                          <li><b>Operaciones Libres: </b>Realiza retiros, depósitos, consultas y transferencias BBVA en todos los canales a nivel nacional.</li>
                          <li><b>Sin costo de mantenimiento:</b> Sin importar el saldo que tengas en tu cue</li>
                      </ul>
                    </vaadin-details>
                  </vaadin-radio-button>
                  
                  <vaadin-radio-button value="CG">
                    <vaadin-details>
                      <div slot="summary">Cuenta Ganadora</div>
                      <ul class="justify__content">
                        <li><b>Premios Exclusivos: </b>Puedes participar de campañas con canje de premios instantáneos y fabulosos sorteos.</li>
                        <li><b>Depósitos ilimitados: </b>En la misma ciudad donde se abrió la cuenta.</li>
                        <li><b>Sin Costo de Mantenimiento: </b>Si mantienes un saldo mayor a S/900 o $300.</li>
                    </ul>
                    </vaadin-details>
                  </vaadin-radio-button>
                </vaadin-radio-group>
              </vaadin-accordion-panel>
              
              <vaadin-accordion-panel theme="reverse" >
                <div slot="summary">2. Datos para obtener la cuenta corriente</div>
                  <vaadin-select id="currency"
                  label="Elige la moneda para tu cuenta" 
                  required
                  error-message="Favor de seleccionar el tipo de moneda."
                  value="{{currency}}">
                    <template>
                      <vaadin-list-box>
                        <vaadin-item value="PEN">Soles</vaadin-item>
                        <vaadin-item value="USD">Dólares</vaadin-item>
                      </vaadin-list-box>
                    </template>
                  </vaadin-select>
              </vaadin-accordion-panel>
            </vaadin-accordion>

            <hr></hr>
            <vaadin-vertical-layout>
              <vaadin-checkbox checked="{{_canSubmit}}">
                He leído y acepto los 
                <a href="#" on-click="toggleDialog">términos y condiciones</a>
              </vaadin-checkbox>
              
              <vaadin-button
                theme="primary"
                disabled$="[[!_canSubmit]]"
                on-click="_submitForm"
                >Aperturar Cuenta
              </vaadin-button>
            </vaadin-vertical-layout>
          </form>
        </iron-form>
      </div>

      <vaadin-notification 
        opened="{{formSubmittedOpen}}" 
        duration="2000"
        position="top-center">
        <template>
          <div>
            <p><b>Apertura de Cuenta</b></p>
            <p>{{message}}</p>
          </div>
        </template>
      </vaadin-notification>

      <vaadin-notification opened="{{formInvalidOpen}}">
        <template>
          <div>
            <p><b>Campos Inválidos</b></p>
            <p>Favor de seleccionar o ingresar los campos requeridos</p>
          </div>
        </template>
      </vaadin-notification>

      <vaadin-dialog
        no-close-on-esc
        no-close-on-outside-click
        opened="{{dialogOpen}}"
      >
        <template>
          <vaadin-vertical-layout theme="spacing">
            <div>
              <p><b>TÉRMINOS Y CONDICIONES</b></p>
              <ol type="a">
                <li>El Banco podrá fijar montos mínimos y máximos para los retiros, depósitos o traspasos entre cuentas, así como para limitar el número de estas operaciones en un determinado plazo, según lo establecido en el Anexo N° 1.</li>
                <li>El Banco podrá entregarle a Usted documentos en los que se registren y consten las operaciones y/o movimientos que se efectúen en la Cuenta. En caso de existir alguna diferencia, prevalecerá la información que conste en los registros del Banco.</li>
                <li>Usted autoriza al Banco a retirar (debitar) de su Cuenta el monto de dinero adelantado por cheques de otros bancos cuyo pago sea rechazado por cualquier motivo. Bajo ningún supuesto el
                    adelanto de pago de tales cheques será entendido como la compra de los mismos por parte del
                    Banco, salvo manifestación expresa y por escrito del Banco en dicho sentido.</li>
                <li>En caso Usted deposite cheques del exterior en su Cuenta, tal(es) depósito(s) estarán sujetos a las disposiciones legales que fuesen aplicables.</li>
                <li>El Banco le pagará los intereses que le correspondan y le cobrará las comisiones y/o gastos que genere la Cuenta, en los montos y con la periodicidad determinada en el Anexo N° 1.</li>
              </ol>
            </div>
            <vaadin-button on-click="toggleDialog">Acepto</vaadin-button>
          </vaadin-vertical-layout>
        </template>
      </vaadin-dialog>
    `;
  }

  static get is() {
    return 'bbva-newaccount-view';
  }

  static get properties() {
    return {
      dialogOpen: Boolean,
      formSubmittedOpen: {
        type: Boolean
      },
      formInvalidOpen: Boolean,
      _canSubmit: Boolean,
      type: String,
      currency: String,
      message: { type: String }
    };
  }

  toggleDialog(e) {
    e.stopPropagation();
    e.preventDefault();
    this.dialogOpen = !this.dialogOpen;
  }

  _cleanForm() {
    this.type = '';
    this.currency = '';
    this._canSubmit = false;
  }

  _submitForm() {
    console.info("_submitForm");
    if (this.$.form.validate()) {
      const token = sessionStorage.getItem(TOKEN);
      axios
        .post(
          URL_PRODUCTS,
          {
            tipo: this.type,
            divisa: this.currency
          },
          {
            headers: {
              'access-token': token
            }
          }
        )
        .then(res => {
          if (res.status === 200) {
            this.message = res.data.message;
            this.formSubmittedOpen = true;
            this._cleanForm();
          }
        })
        .catch(err => {
          this.message = MSG_ERROR;
          this.formSubmittedOpen = true;
        });
    } else {
      this.formInvalidOpen = true;
    }
  }
}

customElements.define(BbvaNewAccountView.is, BbvaNewAccountView);
