import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

import '@polymer/iron-form/iron-form';
import '@vaadin/vaadin-grid/vaadin-grid';
import '@vaadin/vaadin-grid/vaadin-grid-filter-column';
import '@vaadin/vaadin-dialog/vaadin-dialog';
import '@vaadin/vaadin-form-layout/vaadin-form-layout';
import '@vaadin/vaadin-text-field/vaadin-text-field';
import '@vaadin/vaadin-text-field/vaadin-email-field';
import '@vaadin/vaadin-combo-box/vaadin-combo-box';
import '@vaadin/vaadin-lumo-styles/icons';
import '@polymer/iron-icon/iron-icon';
import '@vaadin/vaadin-notification/vaadin-notification';
import '@vaadin/vaadin-button/vaadin-button';

import '../styles/shared-styles';
import * as axios from 'axios';
import {
  URL_USERS,
  TOKEN,
  STATUS_SUCCESS,
  MSG_ERROR
} from '../utils/constants.js';

class BbvaUserMgmtView extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
        }

        vaadin-grid-filter,
        vaadin-text-field {
          width: 100%;
        }

        #btn-add-user {
          margin-top: 1em;
        }

        .button__dialog {
          width: 50%;
        }
      </style>

      <div class="card">
        <h2>Gestión de Usuarios</h2>
        <hr></hr>

        <template is="dom-if" if="[[isFormAddUser]]">
          <!-- <div class="card"> -->
          <h4>Registro de Usuario</h4>
          <iron-form id="form">
            <form>
              <vaadin-form-layout>
                <vaadin-text-field
                  label="Código de Usuario"
                  required
                  maxlength="8"
                  placeholder="ABCDE019"
                  value="{{userCode}}"
                  error-message="Favor de Ingresar un código de Usuario"
                ></vaadin-text-field>
                <vaadin-combo-box
                  label="Perfil"
                  required
                  items="[[profiles]]"
                  value="{{profile}}"
                  error-message="Favor de seleccionar un perfil"
                ></vaadin-combo-box>

                <vaadin-text-field
                  label="Nombres"
                  required
                  value="{{firstname}}"
                  error-message="Favor de Ingresar un nombre para el usuario"
                ></vaadin-text-field>
                <vaadin-text-field
                  label="Apellidos"
                  required
                  value="{{lastname}}"
                  error-message="Favor de Ingresar un apellido para el usuario"
                ></vaadin-text-field>

                <vaadin-text-field
                  label="Número Telefónico"
                  required
                  maxlength="9"
                  value="{{numberPhone}}"
                  error-message="Favor de Ingresar un número válido"
                ></vaadin-text-field>
                <vaadin-combo-box
                  label="Operadora"
                  required
                  items="[[operators]]"
                  value="{{operator}}"
                  error-message="Favor de seleccionar un operador"
                ></vaadin-combo-box>
                
                <vaadin-email-field 
                  label="Email"
                  colspan="2"
                  required
                  value="{{email}}"
                  error-message="Favor de ingresar una dirección de Email válida" 
                  clear-button-visible
                ></vaadin-email-field>

                <vaadin-button id="btn-acept" on-click="_addUser"
                  >Aceptar
                </vaadin-button>
                <vaadin-button theme="error" on-click="_cleanForm"
                  >Cancelar
                </vaadin-button>
              </vaadin-form-layout>
            </form>
          </iron-form>
          <hr></hr>
          <!-- </div> -->
        </template>

        <vaadin-grid items="[[users]]" theme="row-stripes">
          <vaadin-grid-filter-column
            path="codigoUsuario"
            header="Código de Usuario"
          ></vaadin-grid-filter-column>
          <vaadin-grid-filter-column
            path="perfil"
            header="Perfil"
          ></vaadin-grid-filter-column>
          <vaadin-grid-filter-column
            path="nombres"
            header="Nombres"
          ></vaadin-grid-filter-column>
          <vaadin-grid-filter-column
            path="telefono.numero"
            header="Nº de Teléfono"
          ></vaadin-grid-filter-column>
          <vaadin-grid-filter-column path="email"></vaadin-grid-filter-column>
          <vaadin-grid-column header="Opciones">
            <template>
              <vaadin-button aria-label="Edit" theme="icon" focus-target on-click="_openFormEdit">
                <iron-icon icon="lumo:edit"></iron-icon>
              </vaadin-button>
              <vaadin-button aria-label="Delete" theme="icon error" on-click="_openDialog">
                <iron-icon icon="lumo:cross"></iron-icon>
              </vaadin-button>
            </template>
          </vaadin-grid-column>
        </vaadin-grid>
        <vaadin-button id="btn-add-user" 
          theme="primary"
          on-click="toggleFormAddUser">
          Agregar Usuario
        </vaadin-button>
      </div>

      <vaadin-dialog
        no-close-on-esc
        no-close-on-outside-click
        opened="{{dialogOpen}}"
      >
        <template>
          <vaadin-vertical-layout theme="spacing">
              <p>Está seguro que desea eliminar al usuario: <b>{{user.codigoUsuario}} - {{user.nombres}}</b></p>
          </vaadin-vertical-layout>
          <vaadin-button on-click="_deleteUser" class="button__dialog"
            >Aceptar
          </vaadin-button>
          <vaadin-button theme="error" on-click="_closeDialog" class="button__dialog"
            >Cancelar
          </vaadin-button>
        </template>
      </vaadin-dialog>

      <vaadin-notification
        opened="{{notificationOpen}}"
        duration="2000"
        position="top-center"
      >
        <template>
          <div>
            <p><b>Gestión de Usuarios</b></p>
            <p>{{message}}</p>
          </div>
        </template>
      </vaadin-notification>
    `;
  }

  static get is() {
    return 'bbva-usermgmt-view';
  }

  static get properties() {
    return {
      users: { type: Array, value: () => [] },
      user: { type: Object },
      profiles: {
        type: Array,
        value: () => ['ADMIN', 'SOLIDARIO', 'MANCOMUNADO']
      },
      operators: {
        type: Array,
        value: () => ['Movistar', 'Claro', 'Entel', 'Bitel']
      },
      optionEdit: Boolean,
      message: String,
      isFormAddUser: Boolean,
      notificationOpen: Boolean,
      dialogOpen: Boolean,
      userCode: String,
      profile: String,
      firstname: String,
      lastname: String,
      numberPhone: String,
      operator: String,
      email: String
    };
  }

  ready() {
    super.ready();
    this.addEventListener('onload', this._loadUsers(event));
  }

  _loadUsers() {
    console.info('_loadUsers()');
    const jwt = sessionStorage.getItem(TOKEN);

    axios({
      method: 'get',
      url: URL_USERS,
      headers: { 'access-token': jwt }
    })
      .then(res => {
        this.users = res.data;
      })
      .catch(err => console.info(err));
  }

  toggleFormAddUser(e) {
    e.stopPropagation();
    e.preventDefault();
    this.isFormAddUser = !this.isFormAddUser;
    this.optionEdit = false;
  }

  _cleanForm() {
    this.userCode = '';
    this.profile = '';
    this.firstname = '';
    this.lastname = '';
    this.email = '';
    this.numberPhone = '';
    this.operator = '';
    this.isFormAddUser = false;
  }

  _addUser() {
    console.info('_addUser');
    if (!this.optionEdit) {
      const token = sessionStorage.getItem(TOKEN);
      axios
        .post(
          URL_USERS,
          {
            codigoUsuario: this.userCode,
            perfil: this.profile,
            nombres: this.firstname,
            apellidos: this.lastname,
            email: this.email,
            telefono: {
              numero: this.numberPhone,
              operador: this.operator
            }
          },
          {
            headers: {
              'access-token': token
            }
          }
        )
        .then(res => {
          if (res.status === 200) {
            this.message = res.data.message;
            this.notificationOpen = true;
            if (res.data.status === STATUS_SUCCESS) {
              this._loadUsers();
              this._cleanForm();
            }
          }
        })
        .catch(err => {
          this.message = MSG_ERROR;
          this.notificationOpen = true;
        });
    } else {
      this._editUser();
    }
  }

  _openDialog(event) {
    this.user = event.model.__data.item;
    this.dialogOpen = true;
  }

  _closeDialog() {
    this.dialogOpen = false;
  }

  _deleteUser() {
    console.info('_deleteUser()');
    const token = sessionStorage.getItem(TOKEN);
    axios
      .delete(`${URL_USERS}/${this.user.codigoUsuario}`, {
        headers: {
          'access-token': token
        }
      })
      .then(res => {
        if (res.status === 200) {
          this.message = res.data.message;
          this.notificationOpen = true;
          this._loadUsers();
          this._closeDialog();
        }
      })
      .catch(err => {
        this.message = MSG_ERROR;
        this.notificationOpen = true;
      });
  }

  _openFormEdit(event) {
    const user = event.model.__data.item;
    this.optionEdit = true;
    this.isFormAddUser = true;
    /* Seteamos valores a las cajas */
    this.userCode = user.codigoUsuario;
    this.profile = user.perfil;
    this.firstname = user.nombres;
    this.lastname = user.apellidos;
    this.numberPhone = user.telefono.numero;
    this.operator = user.telefono.operador;
    this.email = user.email;
  }

  _editUser() {
    console.info('_editUser');
    const token = sessionStorage.getItem(TOKEN);

    axios
      .put(
        `${URL_USERS}/${this.userCode}`,
        {
          nombres: this.firstname,
          apellidos: this.lastname,
          email: this.email,
          telefono: {
            numero: this.numberPhone,
            operador: this.operator
          }
        },
        {
          headers: {
            'access-token': token
          }
        }
      )
      .then(res => {
        if (res.status === 200) {
          this.message = res.data.message;
          this.notificationOpen = true;
          if (res.data.status === STATUS_SUCCESS) {
            this._cleanForm();
            this._loadUsers();
          }
        }
      })
      .catch(err => {
        this.message = MSG_ERROR;
        this.notificationOpen = true;
      });
  }
}

customElements.define(BbvaUserMgmtView.is, BbvaUserMgmtView);
