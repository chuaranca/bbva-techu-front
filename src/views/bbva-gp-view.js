import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-filter-column.js';
import '@vaadin/vaadin-accordion/vaadin-accordion';
import '@vaadin/vaadin-dialog/vaadin-dialog';
import '../styles/shared-styles.js';

import * as axios from 'axios';
import { URL_PRODUCTS, TOKEN, URL_GET_MOVEMENTS } from '../utils/constants.js';

class BbvaGpView extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
        }

        vaadin-grid-filter,
        vaadin-text-field {
          width: 100%;
        }

        vaadin-grid {
          height: 330px;
        }

        .button__dialog {
          margin-top: 1em;
        }
      </style>
      <div class="card">
        <h2>Posición Global</h2>
        <hr></hr>

        <vaadin-accordion>
          <vaadin-accordion-panel>
            <div slot="summary">Cuenta Fácil</div>
      
            <vaadin-grid items="[[productscf]]" theme="compact row-stripes">
              <vaadin-grid-filter-column
                path="numeroCuenta"
                header="Número de Cuenta"
              ></vaadin-grid-filter-column>
              <vaadin-grid-column
                path="tipo"
                header="Tipo"
              ></vaadin-grid-column>
              <vaadin-grid-column
                path="importeDisponible"
                header="Saldo Disponible"
              ></vaadin-grid-column>
              <vaadin-grid-column
                path="divisa"
                header="Moneda"
              ></vaadin-grid-column>
              <vaadin-grid-column header="Opciones">
                <template>
                  <a on-click="_getMovements">Ver movimientos</a>
                  <!-- <vaadin-button aria-label="Delete" theme="icon error" on-click="_openDialog">
                    <iron-icon icon="lumo:cross"></iron-icon>
                  </vaadin-button> -->
                </template>
              </vaadin-grid-column>
            </vaadin-grid>
          </vaadin-accordion-panel>
        </vaadin-accordion>

        <vaadin-accordion>
          <vaadin-accordion-panel>
            <div slot="summary">Cuenta Ganadora</div>
      
            <vaadin-grid items="[[productscg]]" theme="compact row-stripes">
              <vaadin-grid-filter-column
                path="numeroCuenta"
                header="Número de Cuenta"
              ></vaadin-grid-filter-column>
              <vaadin-grid-column
                path="tipo"
                header="Tipo"
              ></vaadin-grid-column>
              <vaadin-grid-column
                path="importeDisponible"
                header="Saldo Disponible"
              ></vaadin-grid-column>
              <vaadin-grid-column
                path="divisa"
                header="Moneda"
              ></vaadin-grid-column>
              <vaadin-grid-column header="Opciones">
                <template>
                  <a on-click="_getMovements">Ver movimientos</a>
                  <!-- <vaadin-button aria-label="Delete" theme="icon error" on-click="_openDialog">
                    <iron-icon icon="lumo:cross"></iron-icon>
                  </vaadin-button> -->
                </template>
              </vaadin-grid-column>
            </vaadin-grid>
          </vaadin-accordion-panel>
        </vaadin-accordion>
      </div>

      <vaadin-dialog
        no-close-on-esc
        no-close-on-outside-click
        opened="{{openDialogMovements}}"  
      >
        <template>
          <vaadin-vertical-layout style="width: 800px">
            <h3>Consulta de Movimientos</h3>
            <hr></hr>
            <vaadin-grid items="[[movements]]" theme="compact row-stripes">
              <vaadin-grid-column
                auto-width
                path="fechaOperacion"
                header="Fecha de Operación"
              ></vaadin-grid-column>
              <vaadin-grid-column
                auto-width
                path="descripcion"
                header="Descripción"
              ></vaadin-grid-column>
              <vaadin-grid-column
                auto-width
                path="importeOperacion"
                header="Importe"
              ></vaadin-grid-column>
              <vaadin-grid-column
                auto-width
                path="divisa"
                header="Moneda"
              ></vaadin-grid-column>
            </vaadin-grid>

          </vaadin-vertical-layout>
          <vaadin-button on-click="_closeDialog" class="button__dialog"
            >Aceptar
          </vaadin-button>
        </template>
      </vaadin-dialog>
    `;
  }
  static get is() {
    return 'bbva-gp-view';
  }

  static get properties() {
    return {
      productscf: { type: Array, value: [] },
      productscg: { type: Array, value: [] },
      openDialogMovements: Boolean,
      movements: { type: Array, value: [] }
    };
  }

  ready() {
    super.ready();
    this.addEventListener('onload', this._loadProducts(event));
  }

  _loadProducts() {
    console.info('_loadProducts()');
    const jwt = sessionStorage.getItem(TOKEN);
    axios
      .get(URL_PRODUCTS, {
        headers: {
          'access-token': jwt
        }
      })
      .then(res => {
        const listcg = [];
        const listcf = [];
        res.data.forEach(product => {
          const nCuenta = product.numeroCuenta;
          product.numeroCuenta = `${nCuenta.substring(0,4)}-${nCuenta.substring(4, 8)}-${nCuenta.substring(8,10)}-${nCuenta.substring(10)}`;
          product.divisa = product.divisa === 'PEN' ? 'SOLES' : 'DÓLARES';

          if (product.tipo == 'CUENTA GANADORA') {
            listcg.push(product);
          } else {
            listcf.push(product);
          }
        });
        this.productscf = listcf;
        this.productscg = listcg;
      })
      .catch(err => console.info(err));
  }

  _getMovements(event) {
    const token = sessionStorage.getItem(TOKEN);
    const accountNumber = event.model.__data.item.numeroCuenta.replace(/-/g, '');
    this.openDialogMovements = true;
    axios
      .get(`${URL_GET_MOVEMENTS}/${accountNumber}`, {
        headers: {
          'access-token': token
        }
      })
      .then(res => {
        res.data.forEach(mov => {
          mov.fechaOperacion = mov.fechaOperacion.split('T')[0]
        })
        this.movements = res.data;
      })
      .catch(err => console.info(err));
  }

  _closeDialog() {
    this.openDialogMovements = false;
  }
}

customElements.define(BbvaGpView.is, BbvaGpView);
